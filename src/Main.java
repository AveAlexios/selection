public class Main {

// These algorithms are modified to work with Strings
// The program itself consists of one Main class of two methods: main and printing method
// three classes are tree types of soring - Insertion, Bubbles and Direct Selection Sorting
    public static void printing(String[] arrayToPrint)
    {
        for (String element : arrayToPrint)
            System.out.print(element + " ");
        System.out.println();

    }

    public static void main(String[] args) {

// other one more simple array to ensure that the program works fine: "b", "u", "f", "a", "r", "R"
        String[] arr = new String[]{"ein", "zwei", "fuenf", "dreizehn", "siebenundsiebzig"};
        System.out.println("Before sorting: ");
        printing(arr);


        DirectSelectionAlgorithm.sort(arr); //changing the class here you may change the type of sorting you do on arrays
        System.out.println("After sorting: ");
        printing(arr);

    }
}
