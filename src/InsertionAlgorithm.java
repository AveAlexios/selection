public class InsertionAlgorithm {
    public static void sort(String[] arr) {
        for (int i = 1; i < arr.length; i++) {
            String currElem = arr[i];
            int prevKey = i - 1;
            while (prevKey >= 0 && arr[prevKey].compareTo(currElem) > 0) {
                arr[prevKey + 1] = arr[prevKey];
                arr[prevKey] = currElem;
                prevKey--;
            }
        }
    }
}
